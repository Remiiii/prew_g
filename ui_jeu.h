/********************************************************************************
** Form generated from reading UI file 'jeu.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JEU_H
#define UI_JEU_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Jeu
{
public:
    QLabel *label;
    QPushButton *menu_bouton;
    QPushButton *parametre_bouton;

    void setupUi(QWidget *Jeu)
    {
        if (Jeu->objectName().isEmpty())
            Jeu->setObjectName(QString::fromUtf8("Jeu"));
        Jeu->resize(400, 300);
        label = new QLabel(Jeu);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(140, 30, 47, 13));
        menu_bouton = new QPushButton(Jeu);
        menu_bouton->setObjectName(QString::fromUtf8("menu_bouton"));
        menu_bouton->setGeometry(QRect(60, 100, 75, 23));
        parametre_bouton = new QPushButton(Jeu);
        parametre_bouton->setObjectName(QString::fromUtf8("parametre_bouton"));
        parametre_bouton->setGeometry(QRect(260, 100, 75, 23));

        retranslateUi(Jeu);
        QObject::connect(menu_bouton, SIGNAL(clicked()), Jeu, SLOT(Menu()));
        QObject::connect(parametre_bouton, SIGNAL(clicked()), Jeu, SLOT(Parametre()));

        QMetaObject::connectSlotsByName(Jeu);
    } // setupUi

    void retranslateUi(QWidget *Jeu)
    {
        Jeu->setWindowTitle(QCoreApplication::translate("Jeu", "Form", nullptr));
        label->setText(QCoreApplication::translate("Jeu", "Jeu", nullptr));
        menu_bouton->setText(QCoreApplication::translate("Jeu", "Menu", nullptr));
        parametre_bouton->setText(QCoreApplication::translate("Jeu", "Parametre", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Jeu: public Ui_Jeu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JEU_H
