#include "missile.h"

// Constructeur de la classe Misile
Missile::Missile(bool gauche, bool J1)
{

    // On efface les bordures du rectangle de Hitbox

    QPen* p = new QPen();

    p->setStyle(Qt::NoPen);

    this->setPen(*(p));


    setRect(0,0,20,5);

    this->gauche = gauche;

    QTimer* chrono = new QTimer();

    connect(chrono, SIGNAL(timeout()), this, SLOT(move())); // mouvement périodique

    chrono->start(30); // en millisecondes

    this->J1 = J1;


    // Initialisation de l'image des missiles


    QPixmap tirFusil(":/images/Firebolt.png");

    this->comptAnimations  = 1;

    QRect fenetre(48*(comptAnimations),0,48,48);



    if (gauche) {

        QGraphicsPixmapItem* p = new QGraphicsPixmapItem((tirFusil.copy(fenetre)).scaled(QSize(96,96)).transformed(QTransform().scale(-1,1)));

        this->imageMiss = p;

    }

    else {

         QGraphicsPixmapItem* p = new QGraphicsPixmapItem((tirFusil.copy(fenetre)).scaled(QSize(96,96)));

         this->imageMiss = p;

    }


}


// Fonction qui gère le mouvement du Missile
void Missile::move()
{


    QList listeCollision = this->collidingItems();

    // On ignore les collisions avec les joueurs, déjà traitées dans la classe joueur

    foreach(QGraphicsItem * i , listeCollision)
    {

       Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

       Missile * missile = dynamic_cast<Missile *>(i);


       // On ignore toute collision avec un autre objet qu'un missile ou une plateforme

       if (!(missile || plateforme)) {

           listeCollision.remove(listeCollision.indexOf(i));

       }

    }


    if (listeCollision.length()>0) {

        foreach(QGraphicsItem * i , listeCollision)
        {

           Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

           Missile * missile = dynamic_cast<Missile *>(i);

           QGraphicsPixmapItem* p = dynamic_cast<QGraphicsPixmapItem*>(i);




       // Collision entre 2 missiles : destruction des 2




       if (missile) {

           listeCollision.remove(listeCollision.indexOf(i));


           scene()->removeItem(missile->getImageMiss());

           scene()->removeItem(missile);

           delete missile;


           scene()->removeItem(this->getImageMiss());

           scene()->removeItem(this);

           delete this;



       }

       else if (plateforme) {

           listeCollision.remove(listeCollision.indexOf(i));

           scene()->removeItem(this->getImageMiss());

           scene()->removeItem(this);

           delete this;



       }

       else if (p) {

           listeCollision.remove(listeCollision.indexOf(i));

       }

    }


}

else {


    if (this->getGaucheMissile()) {
        setPos(x()-10,y());

        if (pos().x()+rect().width() < 0-100) {

            scene()->removeItem(this->getImageMiss());

            scene()->removeItem(this);



            delete this;
        }

    }

    else {

        setPos(x()+10,y());

        if (pos().x() > scene()->width()+100) {

            scene()->removeItem(this->getImageMiss());

            scene()->removeItem(this);

            delete this;
        }
    }

        }



}


// Getter et Setter
void Missile::setGaucheMissile(bool gauche)
{
    this->gauche = gauche;
}

bool Missile::getGaucheMissile()
{
    return this->gauche;

}

void Missile::setJ1(bool J1) {

    this->J1 = J1;

}

bool Missile::getJ1() {

    return this->J1;

}

void Missile::setImageMiss(QGraphicsPixmapItem* imageMiss) {

    this->imageMiss = imageMiss;


}

QGraphicsPixmapItem* Missile::getImageMiss() {

    return this->imageMiss;
}

void  Missile::setPixMiss(QPixmap* pixMiss) {

    this->pixMiss = pixMiss;

}

QPixmap*  Missile::getPixMiss() {

    return this->pixMiss;

}

int Missile::getComptAnimations() {

    return this->comptAnimations;
}

void Missile::setComptAnimations(int comptAnimations) {

    this->comptAnimations = comptAnimations;

}

