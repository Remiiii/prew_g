#ifndef FUSIL_H
#define FUSIL_H


#include <objetsaramasser.h>

/**
 * Classe qui permet, une fois ramassé de lancer des projectiles
 */
class Fusil : public ObjetsARamasser
{
public:

	// Constructeur par defaut
    Fusil();

};

#endif // FUSIL_H
