#include "hitbox.h"

// Constructeur
Hitbox::Hitbox(Joueur* joueur, bool gauche, bool haut, bool droite, bool bas, int degats)
{

    this->gaucheHit = gauche;
    this->hautHit = haut;
    this->droiteHit = droite;
    this->basHit = bas;

    this->joueur = joueur;
    this->time=0;

    this->degats = degats;


    if (gauche) {

        setRect(0,0,20,100);
        this->paddingGauche= -30;
        this->paddingHaut = 0;
    }


    else if (bas) {

        setRect(0,0,100,20);
        this->paddingHaut = joueur->rect().height()+10;
        this->paddingGauche = 0;
    }

    else if (droite) {

        setRect(0,0,20,100);
        this->paddingGauche= joueur->rect().width()+10;
        this->paddingHaut = 0;
    }

    // Définition de l'attaque neutre (vers le haut)

    else {

        setRect(0,0,100,20);
        this->paddingHaut = -30;
        this->paddingGauche = 0;

    }




    QPen* p = new QPen();

    p->setStyle(Qt::NoPen);

    this->setPen(*(p));



    QTimer* chrono = new QTimer();

    connect(chrono, SIGNAL(timeout()), this, SLOT(attaqueHit())); // mouvement périodique

    chrono->start(30); // en millisecondes
}

// Signal du début de l'attaque
void Hitbox::attaqueHit()
{


    if (!this->joueur->getAttaque()) {

        scene()->removeItem(this);
        delete this;

    }

    else {

    this->time += 30;

    qDebug()<<this->time;


    QList listeCollision = this->collidingItems();


    // On détecte les collisions avec les joueurs


    foreach(QGraphicsItem * i , listeCollision)
    {

    Joueur * adversaire = dynamic_cast<Joueur *>(i);


    if (adversaire) {

        if (adversaire != joueur) {


        listeCollision.remove(listeCollision.indexOf(i));

        adversaire->setVie(adversaire->getVie() - this->degats);

        scene()->removeItem(this);
        delete this;

        }


    }
    }


    if (this->time>=2000) {
        scene()->removeItem(this);
        joueur->setAttaque(false);

        delete this;

    }

    else {

     this->setPos(joueur->x()+paddingGauche, joueur->y()+paddingHaut);

    }

    }

}

// Getter et Setter
void Hitbox::setGaucheHit(bool gaucheHit)
{
    this->gaucheHit = gaucheHit;
}

bool Hitbox::getGaucheHit()
{
    return this->gaucheHit;

}

void Hitbox::setHautHit(bool hautHit)
{
    this->hautHit = hautHit;
}

bool Hitbox::getHautHit()
{
    return this->hautHit;

}

void Hitbox::setDroiteHit(bool droiteHit)
{
    this->droiteHit = droiteHit;
}

bool Hitbox::getDroiteHit()
{
    return this->droiteHit;

}

void Hitbox::setBasHit(bool basHit)
{
    this->basHit = basHit;
}

bool Hitbox::getBasHit()
{
    return this->basHit;

}

void Hitbox::setTime(int time) {

    this->time = time;

}

int Hitbox::getTime() {

    return this->time;
}
