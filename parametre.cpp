#include "parametre.h"
#include "ui_parametre.h"
#include <iostream>

// Constructeur de la classe Parametre
Parametre::Parametre(QWidget *parent) : QWidget(parent), ui(new Ui::Parametre)
{
	// On applique l'interface graphique du fichier Parametre.ui
    ui->setupUi(this);

    // Parametre de l'interface graphique
    ui->Parametres->setStyleSheet("QLabel {color : white; }");
}

// Destructeur par défaut
Parametre::~Parametre()
{
    delete ui;
}

// Changement de fenetre, on passe à la fenetre de MainWindow
void Parametre::Menu()
{
    this->ReturnMenu();
}

// Changement de fenetre, on passe à la fenetre de JEU
void Parametre::Jeu()
{
    this->Play();
}
