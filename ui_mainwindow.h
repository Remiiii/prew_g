/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QStackedWidget *stackedWidget;
    QWidget *Menu;
    QPushButton *quit_bouton;
    QPushButton *joue_bouton;
    QPushButton *parametre_bouton;
    QLabel *label;

    void setupUi(QDialog *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1000, 600);
        MainWindow->setMinimumSize(QSize(1000, 600));
        MainWindow->setMaximumSize(QSize(1000, 600));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/T\303\251l\303\251chargements/Icone.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        stackedWidget = new QStackedWidget(MainWindow);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 1001, 601));
        Menu = new QWidget();
        Menu->setObjectName(QString::fromUtf8("Menu"));
        quit_bouton = new QPushButton(Menu);
        quit_bouton->setObjectName(QString::fromUtf8("quit_bouton"));
        quit_bouton->setGeometry(QRect(460, 340, 75, 23));
        joue_bouton = new QPushButton(Menu);
        joue_bouton->setObjectName(QString::fromUtf8("joue_bouton"));
        joue_bouton->setGeometry(QRect(460, 240, 75, 23));
        parametre_bouton = new QPushButton(Menu);
        parametre_bouton->setObjectName(QString::fromUtf8("parametre_bouton"));
        parametre_bouton->setGeometry(QRect(460, 290, 75, 23));
        label = new QLabel(Menu);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 0, 1001, 601));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/Background.png")));
        label->setScaledContents(true);
        label->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        stackedWidget->addWidget(Menu);
        label->raise();
        quit_bouton->raise();
        joue_bouton->raise();
        parametre_bouton->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QDialog *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Prew-G", nullptr));
        quit_bouton->setText(QCoreApplication::translate("MainWindow", "Quitter", nullptr));
        joue_bouton->setText(QCoreApplication::translate("MainWindow", "Jouer", nullptr));
        parametre_bouton->setText(QCoreApplication::translate("MainWindow", "Param\303\250tres", nullptr));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
