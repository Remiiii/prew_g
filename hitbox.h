#ifndef HITBOX_H
#define HITBOX_H


#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QTimer>
#include <QObject>
#include "joueur.h"


class Hitbox: public QObject, public QGraphicsRectItem {
    Q_OBJECT

public :

    // Constructeur
    Hitbox(Joueur * joueur, bool gauche, bool haut, bool droite, bool bas, int degats);

    // Getter et Setter
    void setGaucheHit(bool gauche);
    bool getGaucheHit();

    void setHautHit(bool haut);
    bool getHautHit();

    void setDroiteHit(bool droiteHit);
    bool getDroiteHit();

    void setBasHit(bool basHit);
    bool getBasHit();

    void setTime(int time);
    int getTime();

public slots:
    // Signal du début de l'attaque
    void attaqueHit();


private:
    // Booléens permettant d'orienter l'attaque
    bool gaucheHit;
    bool hautHit;
    bool droiteHit;
    bool basHit;

    // Timer restant de l'attque, une fois à 0 l'attaque disparais
    int time;

    // Joueur qui a lancé l'attaque
    Joueur* joueur;

    // Padding permettant de positionner l'attaque
    int paddingGauche;
    int paddingHaut;

    // Dégats que l'attque inflige au joueur
    int degats;


};


#endif // HITBOX_H
