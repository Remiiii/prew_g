#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include <QObject>
#include "joueur.h"


class scene: public QGraphicsScene {

Q_OBJECT

public :

   // Constructeur 
   scene(Joueur* j1, Joueur* j2, QWidget* jeu, int timeSpawn);

   // Fonction qui gère les entrées claviers
   void keyPressEvent(QKeyEvent * evenement);
   void keyReleaseEvent(QKeyEvent * evenement);

   // Fonction qui gère la création d'item de façon aléatoire
   void spawnItems();

public slots:
	// Signal qui lance la création d'item
    void spawn();



private :
	
	// Joueur appartenant à la scène
    Joueur* j1;
    Joueur* j2;

    // Indique quel joueur à le focus
    bool joueurActuel; // vaut true si j1 a le focus

    // Timer de chaque Spawn entre les items
    int timeSpawn;

};




#endif // SCENE_H
