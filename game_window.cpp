#include "game_window.h"
#include "jeu.h"
#include "ui_game_window.h"

// Constructeur de la classe Game_Window
game_window::game_window(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::game_window)
{

    // On applique l'interface graphique du fichier game_window.ui
    ui->setupUi(this);

    // Creation du Jeu
    Jeu* game = new Jeu();

    // Parametre de l'interface graphique et ajout du Jeu à la scene
    ui->graphic->setScene(game->sc);
    ui->graphic->setFixedSize(1000,600);
    ui->graphic->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphic->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphic->setStyleSheet("Background : transparent");


}

// Destructeur par défaut
game_window::~game_window()
{
    delete ui;
}

// Changement de fenetre, on passe à la fenetre de MainWindow
void game_window::on_menu_boutton_clicked()
{
    this->ReturnMenu();
}

// Fonction qui permet de recommencer une nouvelle partie
void game_window::on_restart_boutton_clicked()
{
    Jeu* game = new Jeu();
    ui->graphic->setScene(game->sc);
}
