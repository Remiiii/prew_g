#ifndef MISSILE_H
#define MISSILE_H

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QTimer>
#include <QObject>
#include "plateforme.h"
#include "joueur.h"

class Missile: public QObject, public QGraphicsRectItem {
    Q_OBJECT

public :

    // Constructeur de la classe Missile
    Missile(bool gauche = false, bool J1 = true);


    // Getter et Setter
    void setComptAnimations(int comptAnimations);

    int getComptAnimations();    

    void setGaucheMissile(bool gauche);

    bool getGaucheMissile();

    void setJ1(bool J1);

    bool getJ1();

    void setImageMiss(QGraphicsPixmapItem* imageMiss);

    QGraphicsPixmapItem* getImageMiss();

    void  setPixMiss(QPixmap* pixMiss);

    QPixmap*  getPixMiss();




public slots:
    // Fonction qui gère le mouvement du missile
    void move();



private:
    // Indique le sens du missile
    bool gauche;

    // Vaut true si le missile est tiré par le joueur 1
    bool J1;

    // Image du missile
    QPixmap* pixMiss;
    QGraphicsPixmapItem* imageMiss;

    // Compteur de l'animation
    int comptAnimations;

};



#endif // MISSILE_H
