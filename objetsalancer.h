#ifndef OBJETSALANCER_H
#define OBJETSALANCER_H

#include "objets.h"

// Héritage de la Classe Objets
class ObjetsALancer : public virtual Objets
{
public:
    ObjetsALancer();

private:

    int poids;
};

#endif // OBJETSALANCER_H
