#include "objets.h"

// Constructeur de la classe Objets
Objets::Objets()
{
    QPen* p = new QPen();

    p->setStyle(Qt::NoPen);

    this->setPen(*(p));


    QPixmap pp(":/images/Trap.png");


    this->pixObjet = &pp;

    this->imageObjet = new QGraphicsPixmapItem(pp);

    this->comptAnimations = 1;

    // Définition des limites de la Hitbox de l'objet
    this->setRect(100, 100, 50, 50);
}

// Fonction qui gère la gravité des objets
void Objets::Gravite() {


    this->listeCollision = this->collidingItems();


    // On ignore toute collision avec autre chose qu'une plateforme


    foreach(QGraphicsItem * i , listeCollision)
    {
        QGraphicsRectItem* plateforme = dynamic_cast<QGraphicsRectItem *>(i);


        if (!plateforme)
        {

            this->listeCollision.remove(this->listeCollision.indexOf(i));

        }

    }


    // S'il n'y a pas de collision avec une plateforme


    if (this->listeCollision.length()==0) {


       this->setPos(x(), y()+3);


        }

}

// Getter et Setter
void Objets::setImageObjet(QGraphicsPixmapItem* imageObjet) {

    this->imageObjet = imageObjet;

}

QGraphicsPixmapItem* Objets::getImageObjet() {

    return this->imageObjet;

}

void Objets::setPixObjet(QPixmap* pixObjet) {

    this->pixObjet = pixObjet;

}

QPixmap* Objets::getPixObjet() {

    return this->pixObjet;

}

void Objets::setComptAnimations(int comptAnimations) {


    this->comptAnimations = comptAnimations;


}

int Objets::getComptAnimations() {
    return this->comptAnimations;
}








