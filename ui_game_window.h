/********************************************************************************
** Form generated from reading UI file 'game_window.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAME_WINDOW_H
#define UI_GAME_WINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_game_window
{
public:
    QPushButton *menu_boutton;
    QPushButton *restart_boutton;
    QLabel *label;
    QGraphicsView *graphic;

    void setupUi(QWidget *game_window)
    {
        if (game_window->objectName().isEmpty())
            game_window->setObjectName(QString::fromUtf8("game_window"));
        game_window->resize(1000, 600);
        game_window->setMinimumSize(QSize(1000, 600));
        game_window->setMaximumSize(QSize(1000, 600));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/T\303\251l\303\251chargements/Icone.ico"), QSize(), QIcon::Normal, QIcon::Off);
        game_window->setWindowIcon(icon);
        menu_boutton = new QPushButton(game_window);
        menu_boutton->setObjectName(QString::fromUtf8("menu_boutton"));
        menu_boutton->setGeometry(QRect(390, 10, 81, 41));
        menu_boutton->setMinimumSize(QSize(0, 0));
        restart_boutton = new QPushButton(game_window);
        restart_boutton->setObjectName(QString::fromUtf8("restart_boutton"));
        restart_boutton->setGeometry(QRect(480, 10, 71, 41));
        restart_boutton->setMinimumSize(QSize(0, 0));
        label = new QLabel(game_window);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(0, 0, 1001, 601));
        label->setPixmap(QPixmap(QString::fromUtf8(":/images/BackgroundMountains.png")));
        label->setScaledContents(true);
        graphic = new QGraphicsView(game_window);
        graphic->setObjectName(QString::fromUtf8("graphic"));
        graphic->setGeometry(QRect(0, 0, 1000, 600));
        label->raise();
        menu_boutton->raise();
        restart_boutton->raise();
        graphic->raise();

        retranslateUi(game_window);

        QMetaObject::connectSlotsByName(game_window);
    } // setupUi

    void retranslateUi(QWidget *game_window)
    {
        game_window->setWindowTitle(QCoreApplication::translate("game_window", "Form", nullptr));
        menu_boutton->setText(QCoreApplication::translate("game_window", "Pause", nullptr));
        restart_boutton->setText(QCoreApplication::translate("game_window", "Restart", nullptr));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class game_window: public Ui_game_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAME_WINDOW_H
