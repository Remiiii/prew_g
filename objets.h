#ifndef OBJETS_H
#define OBJETS_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QList>
#include "plateforme.h"
#include <iostream>
#include <QtAlgorithms>
#include <QPen>


class Objets : public QGraphicsRectItem
{


public:
    // Constructeur
    Objets();

    // Fonction qui gère la gravite des objets
    void Gravite();

    // Getter et Setter
    void setComptAnimations(int comptAnimations);

    int getComptAnimations();

    void setImageObjet(QGraphicsPixmapItem* imageObjet);

    QGraphicsPixmapItem* getImageObjet();

    void setPixObjet(QPixmap* pixObjet);

    QPixmap* getPixObjet();  



private:

    // Image de l'objet
    QPixmap* pixObjet;
    QGraphicsPixmapItem* imageObjet;

    // Liste des items en collision avec l'objet
    QList<QGraphicsItem*> listeCollision;

    // Compteur de l'animation de l'objet
    int comptAnimations;



};

#endif // OBJETS_H
