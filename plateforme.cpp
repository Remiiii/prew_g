#include "plateforme.h"
#include <QPen>

// Constructeur
Plateforme::Plateforme(int x, int y, int hauteur, int largeur) {

    QPen* p = new QPen();

    p->setStyle(Qt::NoPen);

    this->setPen(*(p));

    this->largeur = largeur;

    this->hauteur = hauteur;

    setRect(x,y,largeur, hauteur);

    QPixmap pp(":/images/Plateforme.png");

    this->pixPlat = &pp;

    this->imagePlat = new QGraphicsPixmapItem(pp.scaled(QSize(largeur+170, hauteur+30)));
    this->imagePlat->setPos(x-85,y-7);

}

// Getter et Setter
void Plateforme::setImagePlat(QGraphicsPixmapItem* imagePlat) {

    this->imagePlat = imagePlat;


}


QGraphicsPixmapItem* Plateforme::getImagePlat() {

    return this->imagePlat;
}


void  Plateforme::setPixPlat(QPixmap* pixPlat) {

    this->pixPlat = pixPlat;

}

QPixmap*  Plateforme::getPixPlat() {

    return this->pixPlat;

}
