QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    bombe.cpp \
    bonus.cpp \
    fusil.cpp \
    game_window.cpp \
    hitbox.cpp \
    imageutils.cpp \
    jeu.cpp \
    joueur.cpp \
    main.cpp \
    mainwindow.cpp \
    missile.cpp \
    objets.cpp \
    objetsalancer.cpp \
    objetsaposer.cpp \
    objetsaramasser.cpp \
    parametre.cpp \
    piege.cpp \
    plateforme.cpp \
    scene.cpp

HEADERS += \
    bombe.h \
    bonus.h \
    fusil.h \
    game_window.h \
    hitbox.h \
    imageutils.h \
    jeu.h \
    joueur.h \
    mainwindow.h \
    missile.h \
    objets.h \
    objetsalancer.h \
    objetsaposer.h \
    objetsaramasser.h \
    parametre.h \
    piege.h \
    plateforme.h \
    scene.h

FORMS += \
    game_window.ui \
    mainwindow.ui \
    parametre.ui

TRANSLATIONS += \
    Prew-G_fr_FR.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
