#ifndef JOUEUR_H
#define JOUEUR_H

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <iostream>
#include <QKeyEvent>
#include <QGraphicsScene>
#include "missile.h"
#include <iostream>
#include <QWidget>
#include <QTimer>
#include <QLabel>
#include <QGraphicsPixmapItem>
#include "fusil.h"
#include "piege.h"



class Hitbox;

class Joueur: public QObject, public QGraphicsRectItem
{

    Q_OBJECT

public:

	// Constructeur de la classe Joueur
    Joueur(bool j1, int x, int y, int width, int height); // le booléen permet de savoir de quel joueur il s'agit (joueur 1, ou joueur 2)

    // Fonction qui gère les entrées claviers
    void KeyboardLinkPress(QKeyEvent *evenement);
    void KeyboardLinkRelease(QKeyEvent* evenement);

    // Fonction qui gère la gravité du joueur
    void Gravite();

    // Fonction qui gère le saut du joueur
    void Saut();

    // Fonction qui vérifie les limites du terrain
    void verifLimits();

    // Fonction qui gère le déplacement du joueur
    void Deplacement();

    // Fonction qui gère la pose d'un Piège
    bool posePiege();

    // Fonction qui gère l'animation des joueurs
    void Animation();

	// Fonction qui gère la gravité des objets
    void GraviteObjets();

    // Fonction qui s'occupe de la vie du Joueur
    void Vie();

    // Fonction qui surcharge l'opérateur <<
    friend std::ostream& operator<<(std::ostream& os, const Joueur* joueur);

    // Getter et Setter
    void setGauche(bool gauche);

    bool getGauche();

    void setHaut(bool haut);

    bool getHaut();

    void setDroite(bool droite);

    bool getDroite();

    void setBas(bool bas);

    bool getBas();

    void setAttaque(bool attaque);

    bool getAttaque();

    bool getSaut();

    void setSaut(bool saut);

    void setValDeplacement(int valDeplacement);

    int getValDeplacement();

    void setCompteurSauts(int compteurSauts);

    int getCompteurSauts();    

    int getToucheGauche();

    void setToucheGauche(int toucheGauche);

    int getToucheDroite();

    void setToucheDroite(int toucheDroite);

    int getToucheHaut();

    void setToucheHaut(int toucheHaut);

    int getToucheBas();

    void setToucheBas(int toucheBas);

    int getToucheSaut();

    void setToucheSaut(int toucheSaut);

    int getToucheMissile();

    void setToucheMissile(int toucheMissile);

    int getToucheAttaque();

    void setToucheAttaque(int toucheAttaque);

    bool getSensJoueur();

    void setSensJoueur(bool sensJoueur);

    bool getJ1();

    Joueur* getAdversaire();

    void setAdversaire(Joueur* adversaire);
    
    void setVie(int vie);

    int getVie();

    void setImageJoueur(QGraphicsPixmapItem* imageJoueur);

    QGraphicsPixmapItem* getImageJoueur();    

    int getValeurTimerSaut();

    void setFusil(bool fusil);

    bool getFusil();

    void setPossedePiege(bool possedePiege);

    bool getPossedePiege();

    void setImageVie(QPixmap imageVie);

    QPixmap getImageVie();

    QGraphicsPixmapItem* getImageVieItem();

    void setImageVieItem(QGraphicsPixmapItem * imageVieItem);

    QList<QGraphicsItem*> getPlateformeSousJoueur();



public slots:
	// Fonction qui lance la gravité
    void Gravity();

    // Fonction qui commence le saut d'un joueur
    void Sauter();

private:
	// Booléens qui gère la direction
    bool gauche;
    bool haut;
    bool droite;
    bool bas;

    bool attaque; // Permet de savoir si le joueur est actuellement en train d'attaquer, pour éviter les conflits entre Hitbox

    int vie;	// Nombre de vie d'un joueur

    bool saut;	// Booléen indiquant si le joueur saute ou pas

    int valeurTimerSaut;	// Durée du saut

    QTimer* timerSaut;	// Timer permettant de gérer le saut

    int valDeplacement;	// Valeur du déplacement du joueur

    int compteurSauts;	// Nombre de saut effectué, 2 au maximum

    int toucheGauche;	// Touche qui permet de se déplacer a GAUCHE

    int toucheDroite;	// Touche qui permet de se déplacer a DROITE

    int toucheHaut;	// Touche qui permet de se déplacer en HAUT

    int toucheBas;	// Touche qui permet de se déplacer en BAS

    int toucheMissile;	// Touche qui permet de lancer un missile

    int toucheSaut;	// Touche qui permet au Joueur de sauter

    int toucheAttaque;	// Touche qui permet au Joueur d'attaquer

    bool sensJoueur; // Vaut true si le joueur est tourné vers la droite, false s'il est tourné vers la gauche

    bool j1;	// Indique si le Joueur est bien J1

    Joueur* adversaire;	// Represente l'adversaire du Joueur

    QList<QGraphicsItem*> listeCollision;	// Liste contenant tous les Items en collision avec le Joueur

    QGraphicsPixmapItem* imageJoueur;	// Image du Joueur

    QGraphicsPixmapItem* imageVieItem;	// Image du fond de la barre de vie du Joueur

    QPixmap imageVie;	// Image de la barre de vie du Joueur


    // Ce booléen indique si le joueur possède un fusil ou non

    bool fusil;

    int valeurTimerFusil;	// Durée pendant laquelle le Joueur peut tirer

    bool possedePiege;		// Booléen indiquant si le Joueur possède ou non un piège

    QList<QGraphicsItem*> plateformeSousJoueur;	// Image de la plateforme



};

#include "hitbox.h"

#endif // JOUEUR_H
