/********************************************************************************
** Form generated from reading UI file 'parametre.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PARAMETRE_H
#define UI_PARAMETRE_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Parametre
{
public:
    QLabel *Parametres;
    QPushButton *menu_bouton;
    QPushButton *joue_bouton;
    QLabel *Image_fond;

    void setupUi(QWidget *Parametre)
    {
        if (Parametre->objectName().isEmpty())
            Parametre->setObjectName(QString::fromUtf8("Parametre"));
        Parametre->resize(1000, 600);
        Parametre->setMinimumSize(QSize(1000, 600));
        Parametre->setMaximumSize(QSize(1000, 600));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/T\303\251l\303\251chargements/Icone.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Parametre->setWindowIcon(icon);
        Parametres = new QLabel(Parametre);
        Parametres->setObjectName(QString::fromUtf8("Parametres"));
        Parametres->setGeometry(QRect(430, 180, 221, 91));
        QFont font;
        font.setPointSize(15);
        font.setBold(true);
        Parametres->setFont(font);
        menu_bouton = new QPushButton(Parametre);
        menu_bouton->setObjectName(QString::fromUtf8("menu_bouton"));
        menu_bouton->setGeometry(QRect(350, 280, 101, 41));
        QFont font1;
        font1.setPointSize(12);
        menu_bouton->setFont(font1);
        joue_bouton = new QPushButton(Parametre);
        joue_bouton->setObjectName(QString::fromUtf8("joue_bouton"));
        joue_bouton->setGeometry(QRect(540, 280, 101, 41));
        joue_bouton->setFont(font1);
        joue_bouton->setIconSize(QSize(20, 20));
        Image_fond = new QLabel(Parametre);
        Image_fond->setObjectName(QString::fromUtf8("Image_fond"));
        Image_fond->setGeometry(QRect(0, 0, 1001, 601));
        Image_fond->setPixmap(QPixmap(QString::fromUtf8(":/images/Background.png")));
        Image_fond->setScaledContents(true);
        Image_fond->raise();
        Parametres->raise();
        menu_bouton->raise();
        joue_bouton->raise();

        retranslateUi(Parametre);
        QObject::connect(menu_bouton, SIGNAL(clicked()), Parametre, SLOT(Menu()));
        QObject::connect(joue_bouton, SIGNAL(clicked()), Parametre, SLOT(Jeu()));

        QMetaObject::connectSlotsByName(Parametre);
    } // setupUi

    void retranslateUi(QWidget *Parametre)
    {
        Parametre->setWindowTitle(QCoreApplication::translate("Parametre", "Form", nullptr));
        Parametres->setText(QCoreApplication::translate("Parametre", "Param\303\250tres", nullptr));
        menu_bouton->setText(QCoreApplication::translate("Parametre", "Menu", nullptr));
        joue_bouton->setText(QCoreApplication::translate("Parametre", "Jouer", nullptr));
        Image_fond->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Parametre: public Ui_Parametre {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARAMETRE_H
