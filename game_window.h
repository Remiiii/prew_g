#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <QWidget>

namespace Ui {
class game_window;
}

class game_window : public QWidget
{
    Q_OBJECT

public:
	// Constructeur de la classe Game_Window
    explicit game_window(QWidget *parent = nullptr);

    // Destructeur par défaut
    ~game_window();

signals:
	// Signal du Retour du menu
    bool ReturnMenu();

private slots:

	// Changement de fenetre, on passe à la fenetre de MainWindow
    void on_menu_boutton_clicked();

    // Fonction qui permet de recommencer une nouvelle partie
    void on_restart_boutton_clicked();

private:
	// Interface graphique de la fenetre de JEU
    Ui::game_window *ui;
};

#endif // GAME_WINDOW_H
