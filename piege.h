#ifndef PIEGE_H
#define PIEGE_H

#include "objetsaposer.h"
#include "objetsaramasser.h"

// Classe qui représente les pièges dans le Jeu, il y a héritage multiple
class Piege : public ObjetsARamasser, public ObjetsAPoser
{

public:
	// Constructeur
    Piege(int degats = 20);

    // Getter et Setter
    int getDegats();

    bool getActive();

    void setActive(bool active);

private :

	// Dégats que le piège inflige au joueur
    int degats;

    // Booléen qui indique si le piège est actif ou non
    bool active;

};

#endif // PIEGE_H
