#include "mainwindow.h"
#include "game_window.h"

// Constructeur de la classe MainWindow
MainWindow::MainWindow(QWidget *parent) : QDialog(parent), ui(new Ui::MainWindow)
{
    // Création de la fenetre de Parametre
    Parametre *parametre = new Parametre();
    // Connexion entre les boutons de navigation et le changement de fenetre
    connect(parametre,SIGNAL(ReturnMenu()),this,SLOT(on_menu_bouton_clicked()));
    connect(parametre,SIGNAL(Play()),this,SLOT(on_joue_bouton_clicked()));

    // Création de la fenetre de Jeu
    game_window *game = new game_window();
    // Connexion entre les boutons de navigation et le changement de fenetre
    connect(game,SIGNAL(ReturnMenu()),this,SLOT(on_menu_bouton_clicked()));

    // On applique l'interface graphique du fichier MainWindow.ui
    ui->setupUi(this);

    // Parametre de l'interface graphique
    ui->stackedWidget->setFixedSize(1000,600);

    // Ajout des fentres de Jeu et de Parametre
    ui->stackedWidget->addWidget(game);
    ui->stackedWidget->addWidget(parametre);
}

// Destructeur par défaut
MainWindow::~MainWindow()
{
    delete ui;
}

// Changement de fenetre, on passe à la fenetre de JEU
void MainWindow::on_joue_bouton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

// Changement de fenetre, on passe à la fenetre de MainWindow
void MainWindow::on_menu_bouton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

// Changement de fenetre, on passe à la fenetre de Parametre
void MainWindow::on_parametre_bouton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

// On quitte le jeu
void MainWindow::on_quit_bouton_clicked()
{
    this->close();
}


