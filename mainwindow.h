#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDialog>
#include <iostream>
#include "ui_mainwindow.h"
#include "parametre.h"
#include "jeu.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QDialog
{
    Q_OBJECT

public:
	// Constructeur de la classe MainWindow
    explicit MainWindow(QWidget *parent = nullptr);

    // Destructeur
    ~MainWindow();

private slots:
	// Changement de fenetre, on passe à la fenetre de JEU
    void on_joue_bouton_clicked();

    // Changement de fenetre, on passe à la fenetre de MainWindow
    void on_menu_bouton_clicked();

    // Changement de fenetre, on passe à la fenetre de Parametre
    void on_parametre_bouton_clicked();

    // On quitte le jeu
    void on_quit_bouton_clicked();

private:
	// Interface graphique de la fenetre MainWindow
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
