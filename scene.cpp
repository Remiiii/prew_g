#include "scene.h"

// Constructeur
scene::scene(Joueur *j1, Joueur *j2, QWidget* jeu, int timeSpawn): QGraphicsScene(jeu)
{
    this->j1 = j1;
    this->j2 = j2;

    this->timeSpawn = timeSpawn;

    this->joueurActuel = true;

}


// Fonction que gère les pressions de claviers
void scene::keyPressEvent(QKeyEvent *evenement) {

    if (evenement->key() == Qt::Key_D || evenement->key() == Qt::Key_S || evenement->key() == Qt::Key_Q  || evenement->key() == Qt::Key_Z
             || evenement->key() == Qt::Key_A  || evenement->key() == Qt::Key_E  || evenement->key() == Qt::Key_W) {

        j1->KeyboardLinkPress(evenement);


    }

    else if (evenement->key() == Qt::Key_Right || evenement->key() == Qt::Key_Down || evenement->key() == Qt::Key_Left  || evenement->key() == Qt::Key_Up
             || evenement->key() == Qt::Key_R  || evenement->key() == Qt::Key_T  || evenement->key() == Qt::Key_C) {

        j2->KeyboardLinkPress(evenement);

    }
}

// Fonction qui gère les relevées des touches
void scene::keyReleaseEvent(QKeyEvent *evenement)
{
    if (evenement->key() == Qt::Key_D || evenement->key() == Qt::Key_S || evenement->key() == Qt::Key_Q  || evenement->key() == Qt::Key_Z
             || evenement->key() == Qt::Key_A  || evenement->key() == Qt::Key_E  || evenement->key() == Qt::Key_W) {

        j1->KeyboardLinkRelease(evenement);

    }

    else if (evenement->key() == Qt::Key_Right || evenement->key() == Qt::Key_Down || evenement->key() == Qt::Key_Left  || evenement->key() == Qt::Key_Up
             || evenement->key() == Qt::Key_R  || evenement->key() == Qt::Key_T  || evenement->key() == Qt::Key_C) {

        j2->KeyboardLinkRelease(evenement);

    }


}

// Foncttion qui lance le spawner d'items
void scene::spawnItems() {

    QTimer* chrono = new QTimer();

    connect(chrono, SIGNAL(timeout()), this, SLOT(spawn())); // mouvement périodique

    chrono->start(this->timeSpawn);

}

// Fonction qui crée les items avec une position aléatoire
void scene::spawn() {

    int randNb = rand()%(2);

    int randX = rand()%(550);


    // On génère un fusil

    if (randNb) {

        Fusil* fusil = new Fusil();

        this->addItem(fusil);

        this->addItem(fusil->getImageObjet());

        fusil->setPos(randX,0-fusil->rect().height());


    }



    // On génère un piège

    else {

        Piege*  piege= new Piege();

        /*QPixmap p(":/images/Trap.png");

        QGraphicsPixmapItem* piegePix = new QGraphicsPixmapItem(p); */

        this->addItem(piege->getImageObjet());
        this->addItem(piege);
        //piege->setImageObjet(piegePix);


        piege->setPos(randX,0-piege->rect().height());



    }



}

