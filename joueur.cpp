#include "joueur.h"

#define VIE 150
#define VALEUR_DEPLACEMENT 2

#define TOUCHE_HAUT_J1 Qt::Key_Z
#define TOUCHE_BAS_J1 Qt::Key_S
#define TOUCHE_DROITE_J1 Qt::Key_D
#define TOUCHE_GAUCHE_J1 Qt::Key_Q
#define TOUCHE_ATTAQUE_J1 Qt::Key_A
#define TOUCHE_SAUT_J1 Qt::Key_W
#define TOUCHE_MISSILE_J1 Qt::Key_E

#define TOUCHE_HAUT_J2 Qt::Key_Up
#define TOUCHE_BAS_J2 Qt::Key_Down
#define TOUCHE_DROITE_J2 Qt::Key_Right
#define TOUCHE_GAUCHE_J2 Qt::Key_Left
#define TOUCHE_ATTAQUE_J2 Qt::Key_R
#define TOUCHE_SAUT_J2 Qt::Key_C
#define TOUCHE_MISSILE_J2 Qt::Key_T



Joueur::Joueur(bool j1, int x, int y, int width, int height)
{


    // On crée un timer pour le saut
    this->timerSaut = new QTimer();
    connect(this->timerSaut, SIGNAL(timeout()), this, SLOT(Sauter())); // mouvement périodique


    // On définit une vitesse de déplacement par défaut

    this->valDeplacement = VALEUR_DEPLACEMENT;

    this->j1 = j1;

    this->setRect(x,y,width,height);


    this->vie = VIE;

    if (j1) { // On utilise les touches z, q, s et d pour déplacer le joueur, a pour attaquer, e pour tirer un missile, et w pour sauter

        this->toucheDroite = TOUCHE_DROITE_J1;
        this->toucheBas = TOUCHE_BAS_J1;
        this->toucheGauche= TOUCHE_GAUCHE_J1;
        this->toucheHaut = TOUCHE_HAUT_J1;

        this->toucheAttaque = TOUCHE_ATTAQUE_J1;
        this->toucheMissile = TOUCHE_MISSILE_J1;
        this->toucheSaut = TOUCHE_SAUT_J1;

        this->setPos(40, 50);

    }

    else { // On utilise les flèches directionnelles pour déplacer le joueur, r pour attaque, t pour tirer un missile, et c pour sauter

        this->toucheDroite = TOUCHE_DROITE_J2;
        this->toucheBas = TOUCHE_BAS_J2;
        this->toucheGauche= TOUCHE_GAUCHE_J2;
        this->toucheHaut = TOUCHE_HAUT_J2;

        this->toucheAttaque = TOUCHE_ATTAQUE_J2;
        this->toucheMissile = TOUCHE_MISSILE_J2;
        this->toucheSaut = TOUCHE_SAUT_J2;

        this->setPos(625, 50);

    }

    this->droite = false;
    this->bas= false;
    this->gauche= false;
    this->haut = false;

    this->fusil = false;

    this->sensJoueur = true;



    QPen* p = new QPen();

    p->setStyle(Qt::NoPen);

    this->setPen(*(p));


}


void Joueur::KeyboardLinkPress(QKeyEvent *evenement){

    // Diagonale Haut-Gauche


    if((evenement->key() == this->toucheGauche and this->getHaut()) ||
            (evenement->key() == this->toucheHaut and this->getGauche()))
    {
        if (!this->attaque) { this->sensJoueur = false; }

        this->setHaut(true);
        this->setGauche(true);
    }

    // Diagonale Bas-Gauche
    else if((evenement->key() == this->toucheGauche and this->getBas()) ||
            (evenement->key() == this->toucheBas and this->getGauche()))
    {
        if (!this->attaque) { this->sensJoueur = false; }

        this->setBas(true);
        this->setGauche(true);
    }

    // Diagonale Haut-Droite
    else if((evenement->key() == this->toucheDroite and this->getHaut()) ||
            (evenement->key() == this->toucheHaut and this->getDroite()))
    {
        if (!this->attaque) { this->sensJoueur = true; }

        this->setHaut(true);
        this->setDroite(true);
    }

    // Diagonale Bas-Droite
    else if((evenement->key() == this->toucheDroite and this->getBas()) ||
            (evenement->key() == this->toucheBas and this->getDroite()))
    {
        if (!this->attaque) { this->sensJoueur = true; }

        this->setBas(true);
        this->setDroite(true);
    }

    // Déplacement à gauche
    else if (evenement->key() == this->toucheGauche) {

        if (!this->attaque) { this->sensJoueur = false; }

        this->setGauche(true);
    }

    // Déplacement en bas
    else if (evenement->key() == this->toucheBas) { this->setBas(true); }

    // Déplacement en haut
    else if (evenement->key() == this->toucheHaut) { this->setHaut(true); }


    // Déplacement à droite
    else if (evenement->key() == this->toucheDroite) {
        if (!this->attaque) { this->sensJoueur = true; }
        this->setDroite(true);
    }

    // On récupère la key et s'il s'agit de la touche de missile et qu'il possède un fusil
    // alors on appelle lance un missile
    if (evenement->key() == this->toucheMissile && this->getFusil()) {

        // On arrête l'attaque en cours
        qDebug()<<"Missile";

        this->attaque = false;


        Missile * missile = new Missile(!this->sensJoueur, this->j1);

        if (!this->sensJoueur) {
            missile->setPos(x()-missile->rect().width()-10,y()+rect().height()/2);
        }
        else {
            missile->setPos(x()+rect().width()+10,y()+rect().height()/2);
        }

        scene()->addItem(missile);

        missile->getImageMiss()->setPos(missile->x()-20, missile->y()-53);

        scene()->addItem(missile->getImageMiss());

    }

    // On récupère la key et s'il s'agit de la touche de saut alors on appelle Saut()
    if (evenement->key() == this->toucheSaut) {

        qDebug()<<"Saut";

        this->attaque = false;

        this->Saut();
    }

    // On récupère la key et s'il s'agit de la touche d'attaque
    if (evenement->key() == this->toucheAttaque) {
        // S'il ne possède pas de piège alors il attaque
        // Sinon il pose le piège
        if (!posePiege()) {
            if (!this->getAttaque()) {

                this->setAttaque(true);

                if (this->getJ1()) {
                    Hitbox * hitbox = new Hitbox(this, this->gauche, this->haut, this->droite, this->bas, 35);
                    scene()->addItem(hitbox);
                }
                else {

                    Hitbox * hitbox = new Hitbox(this, this->gauche, this->haut, this->droite, this->bas, 50);
                    scene()->addItem(hitbox);

                }
            }
        }
    }

}

// Fonction qui pose une piège
bool Joueur::posePiege() {


    if (this->possedePiege)  {

        qDebug()<<"Piege";


        this->attaque = false;


        if (this->getGauche()) {

            Piege*  piege= new Piege();

            QPixmap p(":/images/TrapActivated.png");

            QGraphicsPixmapItem* piegePix = new QGraphicsPixmapItem(p);

            scene()->addItem(piegePix);
            scene()->addItem(piege);
            piege->setImageObjet(piegePix);

            piege->setPos(x()-200, y()-50);
            piege->getImageObjet()->setPos(piege->x()+110, piege->y()+117);

            piege->setActive(true);

        }

        else if (this->droite){

            Piege*  piege= new Piege();

            QPixmap p(":/images/TrapActivated.png");

            QGraphicsPixmapItem* piegePix = new QGraphicsPixmapItem(p);

            scene()->addItem(piegePix);
            scene()->addItem(piege);
            piege->setImageObjet(piegePix);

            piege->setPos(x()+60, y()-50);
            piege->getImageObjet()->setPos(piege->x()+110, piege->y()+117);

            piege->setActive(true);

        }

        this->possedePiege = false;



        return true;


    }

    return false;
}

// Fonction que modifie les variables de direction en fonction de la touche
void Joueur::KeyboardLinkRelease(QKeyEvent *evenement) {

    if (evenement->key() == this->toucheGauche) {

        this->setGauche(false);

    }

    else if (evenement->key() == this->toucheDroite) {

        this->setDroite(false);

    }

    else if (evenement->key() == this->toucheBas) {

        this->setBas(false);

    }


    else if (evenement->key() == this->toucheHaut) {

        this->setHaut(false);

    }
}


// Fonction qui lance la gravité
void Joueur::Gravite(){
    QTimer* chrono = new QTimer();

    connect(chrono, SIGNAL(timeout()), this, SLOT(Gravity())); // mouvement périodique

    chrono->start(10);
}


// Cette fonction peut être décalée dans ImagesUtils directement
void Joueur::Animation() {
    if (this->getJ1()) {
        this->getImageJoueur()->setPos(this->x()-175,this->y()-122);
    }
    else {
        this->getImageJoueur()->setPos(this->x()-142,this->y()-14);
    }
}

// Fonction qui gère l'affichage de la vie
void Joueur::Vie() {
    this->scene()->removeItem(this->imageVieItem);
    this->imageVie = this->imageVie.copy(0,0,this->getVie(),51);
    this->imageVieItem = new QGraphicsPixmapItem(this->imageVie);

    if (this->getJ1()) { this->imageVieItem->setPos(45,31); }
    else { this->imageVieItem->setPos(545,31); }

    this->scene()->addItem(this->imageVieItem);
    this->imageVieItem->setZValue(-1);
}

// La fonction qui applique la gravité aux joueurs
void Joueur::Gravity(){

   this->setZValue(1);

   Animation();

   Vie();

   GraviteObjets();

   Deplacement();

   verifLimits();

   QRect* rectBas = new QRect(x()+5, y()+rect().height(), rect().width()-10, 1);


   QList<QGraphicsItem*> itemsBas = this->scene()->items(*(rectBas));



   foreach(QGraphicsItem * i , itemsBas)
   {
       Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

       if (!plateforme)
       {
           itemsBas.remove(itemsBas.indexOf(i));
       }

   }


    if (this->vie > 0) {

        if (itemsBas.length() == 0) {
            this->setPos(x(), y()+3);
        }

        else {

            if (this->y()>550-this->rect().height()) {
                this->setY(550-this->rect().height());
            }

            if (!this->getSaut()) {
                this->compteurSauts = 0;
            }
            this->setSaut(false);
        }

        if (this->valeurTimerFusil > 0) {

           this->valeurTimerFusil -= 10;

        }
        else {
           this->fusil = false;
        }
    }

    else {
       scene()->removeItem(this->imageJoueur);
       scene()->removeItem(this->imageVieItem);

       delete this->imageJoueur;
       delete this->imageVieItem;

       QPixmap p(":/images/win.png");
       QGraphicsPixmapItem* win = new QGraphicsPixmapItem(p.scaled(QSize(350,180)));
       win->setPos(225,170);

       scene()->addItem(win);
       scene()->removeItem(this);

       delete this;
    }
}

// La fonction qui applique la gravité aux objets
void Joueur::GraviteObjets() {

    QList<QGraphicsItem *> list = scene()->items();

    foreach(QGraphicsItem * i , list)
    {
        Objets* objet = dynamic_cast<Objets *>(i);
        if (objet){objet->Gravite();}
    }

}

// Fonction qui gère les déplacements
void Joueur::Deplacement() {

    // Gestion des collisions entre un joueur et une plateforme, un missile, une hitbox
    QList listeCollision = this->collidingItems();

    // On ignore les collisions entre joueurs, et avec les images
    foreach(QGraphicsItem * i , listeCollision)
    {
        Joueur * joueur = dynamic_cast<Joueur *>(i);
        Missile * missile = dynamic_cast<Missile *>(i);
        QGraphicsPixmapItem * image = dynamic_cast<QGraphicsPixmapItem *>(i);
        Fusil* fusil = dynamic_cast<Fusil *>(i);
        Piege* piege = dynamic_cast<Piege * >(i);


        if (joueur || image)
        {
            listeCollision.remove(listeCollision.indexOf(i));
        }


        else if (missile) {

            listeCollision.remove(listeCollision.indexOf(i));

            // Si le missile est tiré par l'adversaire

            if (missile->getJ1() != this->j1) {

                this->vie = this->vie - 10;

                scene()->removeItem(missile->getImageMiss());

                scene()->removeItem(missile);

                delete missile;

            }
        }


        else if (fusil) {
            qDebug()<<"Fusil";

            this->attaque = false;


            listeCollision.remove(listeCollision.indexOf(i));

            scene()->removeItem(fusil->getImageObjet());
            delete fusil;

            this->fusil = true;

            this->valeurTimerFusil = 5000;


        }

        else if (piege) {

            if (piege->getActive()) {

                this->setVie(this->getVie()-piege->getDegats());

            }

            else {


                this->possedePiege = true;



            }

            this->attaque = false;


            listeCollision.remove(listeCollision.indexOf(i));

            scene()->removeItem(piege->getImageObjet());
            delete piege;




        }

    }


    this->listeCollision = listeCollision;


    // Définition de 4 rectangles pour détecter les collisions avec les plateformes

    QRect* rectGauche = new QRect(x()-1, y()+5, 1, rect().height()-10);
    QRect* rectDroite = new QRect(x()+rect().width(), y()+5, 1, rect().height()-10);
    QRect* rectHaut = new QRect(x()+5, y()-1, rect().width()-10, 1);
    QRect* rectBas = new QRect(x()+5, y()+rect().height(), rect().width()-10, 1);


    // Définition d'un 5ème rectangle pour savoir si le joueur est en chute ou non

    QRect* rectPlateformeSous =  new QRect(x()+5, y()+rect().height(), rect().width()-10, 3);


    // On récupère les éléments sur les côtés du joueur

    QList<QGraphicsItem*> itemsGauche = this->scene()->items(*(rectGauche));

    QList<QGraphicsItem*> itemsDroite = this->scene()->items(*(rectDroite));

    QList<QGraphicsItem*> itemsHaut = this->scene()->items(*(rectHaut));

    QList<QGraphicsItem*> itemsBas = this->scene()->items(*(rectBas));


    this->plateformeSousJoueur = this->scene()->items(*(rectPlateformeSous));


    // On retire de ces listes tout ce qui n'est pas une plateforme

    foreach(QGraphicsItem * i , itemsGauche)
    {
        Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

        if (!plateforme)
        {
            itemsGauche.remove(itemsGauche.indexOf(i));
        }

    }

    foreach(QGraphicsItem * i , itemsDroite)
    {
        Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

        if (!plateforme)
        {
            itemsDroite.remove(itemsDroite.indexOf(i));
        }

    }

    foreach(QGraphicsItem * i , itemsHaut)
    {
        Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

        if (!plateforme)
        {
            itemsHaut.remove(itemsHaut.indexOf(i));
        }

    }

    foreach(QGraphicsItem * i , itemsBas)
    {
        Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

        if (!plateforme)
        {
            itemsBas.remove(itemsBas.indexOf(i));
        }

    }

    foreach(QGraphicsItem * i , this->plateformeSousJoueur)
    {
        Plateforme* plateforme = dynamic_cast<Plateforme *>(i);

        if (!plateforme)
        {
            this->plateformeSousJoueur.remove(this->plateformeSousJoueur.indexOf(i));
        }

    }



    // Diagonale en bas à droite


    if (this->getBas() & this->getDroite()) {


        // Gestion des collisions potentielles


            if (itemsDroite.length() == 0) {

                // Rien à droite ni en bas

                if (itemsBas.length() == 0) {

                     this->setPos(x()+this->valDeplacement,y()+this->valDeplacement);


                }

                // Rien à droite mais collision en bas

                else {

                    this->setPos(x()+this->valDeplacement,y());

                }

            }

            else {

                // Collision à droite mais rien en bas

                if (itemsBas.length() == 0) {

                    this->setPos(x(),y()+this->valDeplacement);

                }


                // Collision à droite et en bas (aucun mouvement)


            }


        }





    // Diagonale en bas à gauche

    if (this->getBas() & this->getGauche()) {

        // Gestion des collisions potentielles


            if (itemsGauche.length() == 0) {

                // Rien à gauche ni en bas

                if (itemsBas.length() == 0) {

                     this->setPos(x()-this->valDeplacement,y()+this->valDeplacement);


                }

                // Rien à gauche mais collision en bas

                else {

                    this->setPos(x()-this->valDeplacement,y());

                }

            }

            else {

                // Collision à gauche mais rien en bas

                if (itemsBas.length() == 0) {

                    this->setPos(x(),y()+this->valDeplacement);

                }


                // Collision à gauche et en bas (aucun mouvement)


            }

    }



    // Direction Droite

    if (this->getDroite() and itemsDroite.length() == 0) {

        this->setPos(x()+this->valDeplacement,y());



    }


    // Direction Bas

    if (this->getBas() and itemsBas.length() == 0) {

            this->setPos(x(),y()+this->valDeplacement);

    }



    // Direction Gauche

    if (this->getGauche() and itemsGauche.length() == 0) {

        this->setPos(x()-this->valDeplacement,y());

    }


}

// Cette fonction permet de faire respawn les joueurs
void Joueur::verifLimits() {
   if (x()< 0-(this->rect().width()+100) || x()>this->scene()->width()+100 || y()+this->rect().height()>this->scene()->height()) {
       this->setPos(this->scene()->width()/2,this->scene()->height()/2);
       this->vie -= 50;
   }
}

// Fonction qui lance le timer des sauts
void Joueur::Saut()
{
    if (compteurSauts <= 1) {
        this->setSaut(true);
        this->valeurTimerSaut = 0;
        this->compteurSauts += 1;
        this->timerSaut->start(10);
    }
}

// Fonction qui réalise le saut et arrête le timer du saut
void Joueur::Sauter()
{

    if (this->valeurTimerSaut>=500) {
        this->timerSaut->stop();
    }
    else {

        if (this->gauche) {
            this->setPos(x()-3,y()-7);
        }

        else if (this->droite) {
            this->setPos(x()+3,y()-7);
        }

        else {
            this->setPos(x(),y()-7);
        }

        this->valeurTimerSaut += 10;
    }
}


// Surchage de l'opérateur <<
std::ostream& operator<<(std::ostream& os, const Joueur* joueur)
{

    if(joueur->j1 == true){
       os << "J1 Vie : " << std::to_string(joueur->vie);
       if(joueur->possedePiege){
           os << " Possede Piege : TRUE";
       }
    }else{
       os << "J2 Vie : " << std::to_string(joueur->vie);
       if(joueur->possedePiege){
           os << " Possede Piege : TRUE";
       }

    }
    return os;
}


// Getter et Setter de tous les attributs de joueur
void Joueur::setGauche(bool gauche)
{
    this->gauche = gauche;
}

bool Joueur::getGauche()
{
    return this->gauche;

}

void Joueur::setHaut(bool haut)
{
    this->haut = haut;
}

bool Joueur::getHaut()
{
    return this->haut;
}


void Joueur::setBas(bool bas)
{
    this->bas = bas;
}

bool Joueur::getBas()
{
    return this->bas;
}

void Joueur::setDroite(bool droite)
{
    this->droite = droite;
}

bool Joueur::getDroite()
{
    return this->droite;
}

void Joueur::setAttaque(bool attaque)
{
    this->attaque = attaque;
}

bool Joueur::getAttaque()
{
    return this->attaque;
}

void Joueur::setSaut(bool saut)
{
    this->saut = saut;
}

bool Joueur::getSaut()
{
    return this->saut;
}

void Joueur::setValDeplacement(int valDeplacement)
{
    this->valDeplacement = valDeplacement;
}

int Joueur::getValDeplacement()
{
    return this->valDeplacement;
}

void Joueur::setCompteurSauts(int compteurSauts) {

     this->compteurSauts = compteurSauts;
}


int Joueur::getCompteurSauts() {
    return this->compteurSauts;
}


int Joueur::getToucheGauche() {
    return this->toucheGauche;
}

void Joueur::setToucheGauche(int toucheGauche) {

    this->toucheGauche = toucheGauche;

}

int Joueur::getToucheDroite() {

    return this->toucheDroite;
}

void Joueur::setToucheDroite(int toucheDroite) {

    this->toucheDroite = toucheDroite;
}


int Joueur::getToucheHaut() {

    return this->toucheHaut;
}

void Joueur::setToucheHaut(int toucheHaut) {

    this->toucheHaut = toucheHaut;

}

int Joueur::getToucheBas() {

    return this->toucheBas;
}

void Joueur::setToucheBas(int toucheBas) {

    this->toucheBas = toucheBas;

}


int Joueur::getToucheSaut() {

    return this->toucheSaut;
}


void Joueur::setToucheSaut(int toucheSaut) {

    this->toucheSaut = toucheSaut;
}


int Joueur::getToucheMissile() {

    return this->toucheMissile;
}


void Joueur::setToucheMissile(int toucheMissile) {

    this->toucheMissile = toucheMissile;

}


int Joueur::getToucheAttaque() {

    return this->toucheAttaque;
}


void Joueur::setToucheAttaque(int toucheAttaque) {

    this->toucheAttaque = toucheAttaque;
}


bool Joueur::getSensJoueur() {

    return this->sensJoueur;

}

void Joueur::setSensJoueur(bool sensJoueur) {

    this->sensJoueur = sensJoueur;

}


bool Joueur::getJ1() {

    return this->j1;
}


Joueur* Joueur::getAdversaire() {

    return this->adversaire;

}


void Joueur::setAdversaire(Joueur* adversaire) {

    this->adversaire = adversaire;

}

void Joueur::setVie(int vie) {

    this->vie = vie;
}

int Joueur::getVie() {

    return this->vie;
}

void Joueur::setImageJoueur(QGraphicsPixmapItem* imageJoueur) {

    this->imageJoueur = imageJoueur;
}

QGraphicsPixmapItem* Joueur::getImageJoueur() {

   return this->imageJoueur;

}


int Joueur::getValeurTimerSaut() {

    return this->valeurTimerSaut;


}


void Joueur::setFusil(bool fusil) {

    this->fusil = fusil;
}


bool Joueur::getFusil() {

   return this->fusil;

}


void Joueur::setPossedePiege(bool possedePiege) {

    this->possedePiege = possedePiege;


}


bool Joueur::getPossedePiege() {

    return this->possedePiege;

}



void Joueur::setImageVie(QPixmap imageVie) {

    this->imageVie = imageVie;

}


QPixmap Joueur::getImageVie() {

    return this->imageVie;

}


QGraphicsPixmapItem* Joueur::getImageVieItem() {

    return this->imageVieItem;

}

void Joueur::setImageVieItem(QGraphicsPixmapItem * imageVieItem) {

    this->imageVieItem = imageVieItem;
}

QList<QGraphicsItem*> Joueur::getPlateformeSousJoueur() {

    return this->plateformeSousJoueur;

}


