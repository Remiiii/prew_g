#ifndef PARAMETRE_H
#define PARAMETRE_H

#include <QWidget>

namespace Ui {
class Parametre;
}

class Parametre : public QWidget
{
    Q_OBJECT

public:
	// Constructeur de la classe Parametre
    explicit Parametre(QWidget *parent = nullptr);

    // Destructeur
    ~Parametre();

signals:
	//Signal du retour au Menu
    bool ReturnMenu();
    
    //Signal du retour au Jeu
    bool Play();

private slots:
	// Changement de fenetre, on passe à la fenetre de MainWindow
    void Menu();

    // Changement de fenetre, on passe à la fenetre de JEU
    void Jeu();

private:
	// Interface graphique de la fenetre Parametre
    Ui::Parametre *ui;
};

#endif // PARAMETRE_H
