#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <QPixmap>
#include <QLabel>
#include <QObject>
#include "scene.h"

class Imagesutils: public QObject {

    Q_OBJECT

public :

    // Constructeur de la classe ImageUtils
    Imagesutils(scene* sc);

    // Fonction qui lance la boucle d'animation
    void Pre_Animation();

    // Fonction qui gère l'animation d'un Joueur
    void AnimationJoueur(Joueur* j1);

    // Fonction qui gère l'animation d'un missile
    void AnimationMissile();

    // Fonction qui gère l'animation d'une Hitbox
    void AnimationHitbox();

    // Fonction qui gère l'animation d'un fusil
    void AnimationFusil(Fusil* fusil);

    // Fonction qui gère l'animation d'un piege
    void AnimationPiege(Piege* piege);

    // Getter de l'image de fond pour la barre de vie
    QPixmap getFondVie();

    // Getter de l'image de la barre de vie
    QPixmap* getBarreVie();

    // Setter de la barre de vie
    void setBarreVie(QPixmap* barreVie);

    // Fonction qui gère l'animation d'un missile spécifique
    void AnimationMissile(Missile * missile);

public slots:
    // Signal de l'animation
    void Animation();

private :
        
    // Scene contentant toutes les animations
    scene* sc;

    // Image représentant le J1
    QPixmap J1;

    // Image représentant le J2
    QPixmap J2;

    // Compteur d'animation pour J1
    int comptJ1;

    // Compteur d'animation pour J2
    int comptJ2;

    // Compteur d'animation pour les missiles
    int comptMissile;

    // Compteur d'animation pour les Hitbox
    int comptHitbox;

    // Rectangle qui correspond à l'image affichée
    QRect * fenetreCrop;

    // Permet de faire une découpe dans la SpriteSheet
    QGraphicsPixmapItem* imageCrop;

    // Image représentant le fond de la barre de vie
    QPixmap fondVie;

    // Image représentant la barre de vie
    QPixmap* barreVie2;

    // Image représentant le piege
    QPixmap* piege;

    // Liste d'image représentant les fusils
    QList<QPixmap*> listeGun;

    // Image représentant le projectile
    QPixmap tirFusil;



};

#endif // IMAGEUTILS_H
