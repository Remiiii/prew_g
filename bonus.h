#ifndef BONUS_H
#define BONUS_H


#include <objetsaramasser.h>

/**
 * La classe Bombe pas encore implémentée
 */
class Bonus : public ObjetsARamasser
{
public:
    Bonus();

private:

    int bonusVie;
    int bonusAttaque;
    int bonusVitesse;
};

#endif // BONUS_H
