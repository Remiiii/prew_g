#ifndef JEU_H
#define JEU_H

#include <QWidget>
#include <QApplication>
#include<QGraphicsView>
#include<QGraphicsScene>
#include "joueur.h"
#include "plateforme.h"
#include "scene.h"
#include "piege.h"
#include "imageutils.h"


class Jeu : public QWidget
{
    Q_OBJECT

public:
	// Constructeur de la classe Parametre
    explicit Jeu(QWidget *parent = nullptr);

    // Lancement et parametrage de la partie
    void Lancer();

    // Scene du Jeu
    scene* sc;
};

#endif // JEU_H
