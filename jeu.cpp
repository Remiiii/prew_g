#include "jeu.h"
#include "ui_jeu.h"
#include <QDebug>

// Constructeur de la classe Jeu
Jeu::Jeu(QWidget *parent) : QWidget(parent) {
    Lancer();
}

// Fonction qui crée le Jeu
void Jeu::Lancer() {


    // On crée un joueur (dérivé d'un rectangle) pour la hitbox
    Joueur* j1 = new Joueur(true, 0, 0, 100, 100);

    // On crée un second joueur (dérivé d'un rectangle) pour la hitbox
    Joueur* j2 = new Joueur(false, 0, 0, 100, 100);

    // On parametre les adversaires de chaque joueurs
    j1->setAdversaire(j2);
    j2->setAdversaire(j1);

    // On affiche les deux joueurs dans la sortie standard
    std::cout << j1 << std::endl;
    std::cout << j2 << std::endl;



    // On crée une scène avec les deux joueurs
    this->sc = new scene(j1,j2, this, 10000);

    // On crée un gestionnaire des images avec la scène
    Imagesutils* images = new Imagesutils(this->sc);

    // On récupère l'image du joueur J1
    QRect rect(0,0,224,112);
    QPixmap original(":/images/J1.png");
    QPixmap cropped = original.copy(rect);
    QGraphicsPixmapItem* imagej1 = new QGraphicsPixmapItem(cropped);

    // On l'affiche et on enregistre l'image dans le joueur J1
    this->sc->addItem(imagej1);
    j1->setImageJoueur(imagej1);

    // On positionne l'image
    imagej1->setPos(j1->x(),j1->y());

    // On récupère l'image du joueur J2
    QRect rect2(0,0,100,64);
    QPixmap original2(":/images/J2.png");
    QPixmap cropped2 = original2.copy(rect2);
    QGraphicsPixmapItem* imagej2 = new QGraphicsPixmapItem(cropped2);

    // On l'affiche et on enregistre l'image dans le joueur J2
    this->sc->addItem(imagej2);
    j2->setImageJoueur(imagej2);

    // On positionne l'image
    imagej2->setPos(j2->x(),j2->y());

    // Création et ajout d'un premier fusil
    Fusil* fusil = new Fusil();
    this->sc->addItem(fusil);
    this->sc->addItem(fusil->getImageObjet());
    fusil->setPos(300,300);

    // Ajout du fond de la barre de vie pour le joueur J1
    QGraphicsPixmapItem* fondV = new QGraphicsPixmapItem(images->getFondVie());
    this->sc->addItem(fondV);
    fondV->setPos(0,30);
    fondV->setZValue(-2);


    // Ajout du fond de la barre de vie pour le joueur J2
    QGraphicsPixmapItem* fondV2 = new QGraphicsPixmapItem(images->getFondVie());
    this->sc->addItem(fondV2);
    fondV2->setPos(500,30);
    fondV2->setZValue(-2);


    // Ajout d'une barre de vie pleine pour le joueur J1
    QPixmap fondviepix2(":/images/barreVieRouge.png");
    fondviepix2 = fondviepix2.scaled(QSize(150, 51));

    QGraphicsPixmapItem* fondVie2 = new QGraphicsPixmapItem(fondviepix2);

    this->sc->addItem(fondVie2);
    j1->setImageVie(fondviepix2);
    j1->setImageVieItem(fondVie2);
    fondVie2->setPos(45,31);


    // Ajout d'une barre de vie pleine pour le joueur J2


    QPixmap fondviepix3(":/images/barreVieRouge.png");
    fondviepix3 = fondviepix3.scaled(QSize(150, 51));

    QGraphicsPixmapItem* imgVie = new QGraphicsPixmapItem(fondviepix3);

    this->sc->addItem(imgVie);
    j2->setImageVie(fondviepix3);
    j2->setImageVieItem(imgVie);
    imgVie->setPos(545,31);

    // Parametrage de la Scene du jeu
    this->sc->setSceneRect(0,0,800,600);


    // On crée et on ajoute une plateforme centrale
    Plateforme* plateforme = new Plateforme(0, 550, 20, 780);



    // On crée 2 plateformes supérieures
    Plateforme* plateforme2 = new Plateforme(30, 300, 20, 120);
    Plateforme * plateforme3 = new Plateforme(650, 180, 20, 50);

    // Affichage des images des plateformes
    this->sc->addItem(plateforme->getImagePlat());
    this->sc->addItem(plateforme2->getImagePlat());
    this->sc->addItem(plateforme3->getImagePlat());

    // Modification de la vitesse de J2
    j2->setValDeplacement(1);

    // On active la gravité pour les 2 joueurs (-> constructeur)
    j1->Gravite();
    j2->Gravite();

    // On met le focus sur la scene
    this->sc->setFocus();

    //On ajoute les 2 joueurs et les plateformes à la scène
    this->sc->addItem(j1);
    this->sc->addItem(j2);

    this->sc->addItem(plateforme);
    this->sc->addItem(plateforme2);
    this->sc->addItem(plateforme3);

    // Lancement des chargements des images en mémoire
    images->Pre_Animation();

    // On lance l'apparition des items automatiquement
    this->sc->spawnItems();
}
