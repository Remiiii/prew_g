#ifndef PLATEFORME_H
#define PLATEFORME_H

#include <QGraphicsRectItem>

// Classe qui représente les plateformes dans le JEU
class Plateforme: public QGraphicsRectItem {

public :
    
    // Constructeur
    Plateforme(int x, int y, int hauteur, int largeur);

    // Getter et Setter
    void setHauteur(int hauteur);

    int getHauteur();

    void setLargeur(int largeur);

    int getLargeur();

    void setImagePlat(QGraphicsPixmapItem* imagePlat);

    QGraphicsPixmapItem* getImagePlat();

    void setPixPlat(QPixmap* pixPlat);

    QPixmap* getPixPlat();



private :
    
    // Paramètre de la platerforme
    int hauteur;
    int largeur;

    // Image de la plateforme
    QPixmap* pixPlat;
    QGraphicsPixmapItem* imagePlat;

};


#endif // PLATEFORME_H
