#include "mainwindow.h"

#include <QApplication>
#include<QGraphicsView>
#include<QGraphicsScene>

int main(int argc, char *argv[])
{
	// Lancement de notre jeu
    QApplication app(argc, argv);

    // Création d'un widget qui servira de fenêtre
    MainWindow fenetre;

    // On affiche la fenetre de notre application
    fenetre.show();


    return app.exec();
}
