#include "imageutils.h"


// Le constructeur de la classe Imagesutils, qui charge les images utiles à l'affichage des animations

Imagesutils::Imagesutils(scene* sc) {

    this->sc = sc;


    // Chargement des spritesheets des joueurs

    this->J1 = QPixmap(":/images/J1.png");
    this->J2 = QPixmap(":/images/J2.png");



    // Chargement de l'image de fond de la barre de vie


    QPixmap fondviepix(":/images/barreVieFond.png");
    this->fondVie = fondviepix;

    this->fondVie = this->fondVie.scaled(QSize(200, 50));


    // Chargement de l'image de la barre de vie pleine

    QPixmap barreVie(":/images/barreVieRouge.png");
    barreVie = barreVie.scaled(QSize(150, 51));

    this->barreVie2 = &barreVie;


    // Chargement de l'image des pièges activés

    QPixmap trapActivated(":/images/TrapActivated.png");
    this->piege = &trapActivated;


    // Chargement du spritesheet des tirs du pistolet

    QPixmap tirFusil(":/images/Firebolt.png");
    this->tirFusil =  tirFusil;





    // Chargement des images de l'animation de la colonne de feu (spawn d'un pistolet)
    QPixmap* gun1 = new QPixmap(":/images/fire_column_medium_1.png");
    QPixmap* gun2 = new QPixmap(":/images/fire_column_medium_2.png");
    QPixmap* gun3 = new QPixmap(":/images/fire_column_medium_3.png");
    QPixmap* gun4 = new QPixmap(":/images/fire_column_medium_4.png");
    QPixmap* gun5 = new QPixmap(":/images/fire_column_medium_5.png");
    QPixmap* gun6 = new QPixmap(":/images/fire_column_medium_6.png");
    QPixmap* gun7 = new QPixmap(":/images/fire_column_medium_7.png");
    QPixmap* gun8 = new QPixmap(":/images/fire_column_medium_8.png");
    QPixmap* gun9 = new QPixmap(":/images/fire_column_medium_9.png");
    QPixmap* gun10 = new QPixmap(":/images/fire_column_medium_10.png");
    QPixmap* gun11 = new QPixmap(":/images/fire_column_medium_11.png");
    QPixmap* gun12 = new QPixmap(":/images/fire_column_medium_12.png");
    QPixmap* gun13 = new QPixmap(":/images/fire_column_medium_13.png");
    QPixmap* gun14 = new QPixmap(":/images/fire_column_medium_14.png");

    // Liste de toutes les images
    QList<QPixmap*> liste;

    // On ajoute toutes les images dans la liste
    liste.append(gun1);
    liste.append(gun2);
    liste.append(gun3);
    liste.append(gun4);
    liste.append(gun5);
    liste.append(gun6);
    liste.append(gun7);
    liste.append(gun8);
    liste.append(gun9);
    liste.append(gun10);
    liste.append(gun11);
    liste.append(gun12);
    liste.append(gun13);
    liste.append(gun14);

    this->listeGun = liste;

}



// La fonction Pre_Animation lance la boucle d'animation (avec un taux de rafraîchissement des images de 65 ms)

void Imagesutils::Pre_Animation() {


    QTimer* chrono = new QTimer();

    connect(chrono, SIGNAL(timeout()), this, SLOT(Animation()));

    chrono->start(65); // en millisecondes


}



// La fonction Animation s'occupe d'animer les joueurs, les missiles, les fusils ainsi que les pièges présents dans la scène

void Imagesutils::Animation() {


    QList<QGraphicsItem *> liste = this->sc->items();


    foreach(QGraphicsItem * i , liste)
    {
        Joueur * joueur = dynamic_cast<Joueur *>(i);
        Missile * missile = dynamic_cast<Missile *>(i);
        Fusil* fusil = dynamic_cast<Fusil *>(i);
        Piege* piege = dynamic_cast<Piege* >(i);


        if (joueur)
        {
            AnimationJoueur(joueur);

            }


        else if (missile) {

            AnimationMissile(missile);


        }

        else if (fusil) {

          AnimationFusil(fusil);

        }

        else if (piege) {

            AnimationPiege(piege);
        }

    }

}



// La fonction AnimationMissile se charge d'animer les missiles de la scène avec les 4 animations appropriées, récupérées sur la spritesheet

void Imagesutils::AnimationMissile(Missile * missile) {

    if (missile->getComptAnimations() > 3) {

        missile->setComptAnimations(0);

    }


        missile->scene()->removeItem(missile->getImageMiss());


        this->fenetreCrop = new QRect(48*(missile->getComptAnimations()),0,48,48);



        if (missile->getGaucheMissile()) {

            QGraphicsPixmapItem* p = new QGraphicsPixmapItem((this->tirFusil.copy((*(this->fenetreCrop)))).scaled(QSize(96,96)).transformed(QTransform().scale(-1,1)));

            missile->setImageMiss(p);

        }

        else {

             QGraphicsPixmapItem* p = new QGraphicsPixmapItem((this->tirFusil.copy((*(this->fenetreCrop)))).scaled(QSize(96,96)));

             missile->setImageMiss(p);

        }




        // On replace l'image correctement par rapport à la hitbox

        missile->getImageMiss()->setPos(missile->x()-20, missile->y()-53);


        // On ajoute l'image à la scène

        missile->scene()->addItem(missile->getImageMiss());

        missile->setComptAnimations(missile->getComptAnimations() +1);
}



// La fonction AnimationPiege se contente de replacer l'image du piège correctement par rapport à sa hitbox (le piège n'est pas animé)

void Imagesutils::AnimationPiege(Piege* piege) {
    piege->getImageObjet()->setPos(piege->x() + 110, piege->y() + 116);
}



// La fonction AnimationFusil se charge d'animer les fusils de la scène avec les 14 animations appropriées, récupérées indépendamment

void Imagesutils::AnimationFusil(Fusil* fusil) {
    if (fusil->getComptAnimations() > 14) {
        fusil->setComptAnimations(1);
    }

    fusil->scene()->removeItem(fusil->getImageObjet());


    // On charge l'image d'intérêt en parcourant la liste à chaque tour
    QPixmap pp = *(this->listeGun).at(fusil->getComptAnimations()-1);
    fusil->setPixObjet(&pp);


    QGraphicsPixmapItem* p = new QGraphicsPixmapItem(pp);
    fusil->setImageObjet(p);


    // On replace l'image correctement par rapport à sa hitbox
    fusil->getImageObjet()->setPos(fusil->x()+102, fusil->y()+65);


    // On ajoute l'image à la scène
    fusil->scene()->addItem(fusil->getImageObjet());

    fusil->setComptAnimations(fusil->getComptAnimations() +1);
}



// La fonction AnimationJoueur se charge d'animer tout joueur présent sur la scène avec les spritesheets associées

void Imagesutils::AnimationJoueur(Joueur* j1) {

    j1->scene()->removeItem(j1->getImageJoueur());


    // Animation du Joueur 1


    if (j1->getJ1()) {


        // Animation du saut et de la chute du joueur

        if ((j1->getValeurTimerSaut()!=0 && j1->getCompteurSauts()!=0) || (j1->getPlateformeSousJoueur().length()==0)) {


            if (this->comptJ1>2) { this->comptJ1 = 0; }


            this->fenetreCrop = new QRect(224*comptJ1,336,224,112);


        }


        // Animation du joueur lorsqu'il est au repos ou en train de courir

        else if (!j1->getAttaque()) {


            // Si on a affiché les 8 animations du joueur 1 (que ce soit au repos ou en course), alors on recommence la boucle

            if (this->comptJ1>7) { this->comptJ1 = 0; }


            // Soit le joueur court, soit il est au repos

            if (j1->getDroite() || j1->getGauche()) { this->fenetreCrop = new QRect(224*comptJ1,112,224,112); }

            else { this->fenetreCrop = new QRect(224*comptJ1,0,224,112); }

        }



        // Animation du joueur en train d'attaquer

        else {

            if (this->comptJ1>17) {
                this->comptJ1 = 0;
                j1->setAttaque(false);
            }


            this->fenetreCrop = new QRect(224*comptJ1,6*112,224,112);


        }



        // On inverse l'image obtenue si le joueur est tourné vers la gauche, et on la redimensionne (2 fois plus grande sur chaque axe)

        if (!j1->getSensJoueur()) {

            this->imageCrop = new QGraphicsPixmapItem((this->J1.copy(*(this->fenetreCrop))).scaled(QSize(448, 224)).transformed(QTransform().scale(-1, 1)));

        }


        else {

            this->imageCrop = new QGraphicsPixmapItem((this->J1.copy(*(this->fenetreCrop))).scaled(QSize(448, 224)));

        }




        this->comptJ1++;


    }



    // Animation du Joueur 2


    else {

        // Animation de la chute et du saut du joueur

        if (j1->getValeurTimerSaut()!=0 & j1->getCompteurSauts()!=0 || j1->getPlateformeSousJoueur().length()==0) {


            if (this->comptJ2>2) { this->comptJ2 = 0; }


            this->fenetreCrop = new QRect(192*comptJ2,192,192,64);


            }


        // Animation du joueur en attente ou en course

        else if (!j1->getAttaque()) {


            // Animation de la course du joueur

            if (j1->getDroite() || j1->getGauche()) {


                // Si on a affiché les 8 animations de course du joueur 2, alors on recommence la boucle

                if (this->comptJ2>7) { this->comptJ2 = 0; }

                this->fenetreCrop = new QRect(192*comptJ2,64,192,64);

            }


            // Animation de l'attente du joueur

            else {

                // Si on a affiché les 6 animations de repos joueur 1, alors on recommence la boucle

                if (this->comptJ2>5) { this->comptJ2 = 0; }

                this->fenetreCrop = new QRect(192*comptJ2,0,192,64);

            }

       }




     else {


        if (this->comptJ2>24) {

            this->comptJ2 = 0;
            qDebug()<<"Animation Fin";
            j1->setAttaque(false);
        }


        this->fenetreCrop = new QRect(192*comptJ2,7*64,192,64);


    }



    if (!j1->getSensJoueur()) {

        this->imageCrop = new QGraphicsPixmapItem((this->J2.copy(*(this->fenetreCrop))).scaled(QSize(384, 128)).transformed(QTransform().scale(-1, 1)));

    }


    else {

        this->imageCrop = new QGraphicsPixmapItem((this->J2.copy(*(this->fenetreCrop))).scaled(QSize(384, 128)));

    }


    this->comptJ2++;


    }


    j1->setImageJoueur(this->imageCrop);


    j1->Animation();

    j1->scene()->addItem(j1->getImageJoueur());




}


// Getters et Setters
QPixmap Imagesutils::getFondVie() {

    return this->fondVie;

}


QPixmap* Imagesutils::getBarreVie() {

    return this->barreVie2;

}


void Imagesutils::setBarreVie(QPixmap* barreVie) {

    this->barreVie2 = barreVie;

}


