******************
*     Prew-G     *
******************

Pour exécuter le programme via l'installer (sous Windows):

	1. Cloner le répertoire https://gitlab.com/Remiiii/prew_g via la commande "git clone https://gitlab.com/Remiiii/prew_g"
	2. Déplacer vous dans le dossier prew-g
	3. Déplacer vous dans le dossier installer
	3. Executer Prew-G.exe
	4. Lancer le jeu et Amusez-vous

Pour exécuter le programme via Qt Creator:
	1. Cloner le répertoire https://gitlab.com/Remiiii/prew_g via la commande "git clone https://gitlab.com/Remiiii/prew_g"
	2. Ouvrir le logiciel Qt Creator
	2. Ouvrir le fichier prew-g/Prew-G.pro
	3. Compiler le projet


*********************************
* Auteurs :						*
*								*
* Quentin CARLIER				*
* Rémi BARBIER					*
*********************************