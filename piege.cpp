#include "piege.h"

// Constructeur
Piege::Piege(int degats) : ObjetsAPoser()
{

    this->degats = degats;
    this->active = false;

}

// Getter et Setter
int Piege::getDegats() {

    return this->degats;


}

bool Piege::getActive() {

    return this->active;

}

void Piege::setActive(bool active) {

    this->active = active;
}
